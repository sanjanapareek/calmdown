package com.example.android.calmdown;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.List;
import java.util.Random;

/**
 * Created by sanjana.pareek on 27/05/16.
 */
public class SurpriseActivity extends Activity{
    int min;
    int max;
    Intent intent;
    Uri uri;
    ImageView imageView;
    Drawable res;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.surprise_me);
         min = 0;
         max = 9;

        Random random = new Random();
        int option = random.nextInt(max - min + 1) + min;

        Log.i("SurpriseList", "Random value:" + option);

        switch (option){
            case 0: imageView = (ImageView) findViewById(R.id.surprise_image);
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.surpriseme1));
                break;

            case 1: uri = Uri.parse("https://www.youtube.com/watch?v=jZViOEv90dI");
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;

            case 2: uri = Uri.parse("https://www.youtube.com/watch?v=PydJUinuXHc");
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;

            case 3: uri = Uri.parse("https://www.youtube.com/watch?v=RHj5-CnaqIs");
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;

            case 4: uri = Uri.parse("https://www.youtube.com/watch?v=VRkJ930L_kE");
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;

            case 5: uri = Uri.parse("https://www.youtube.com/watch?v=3Xeh9JPO4Co&index=2&list=PLIIhFdIwULxKicqb0dqI69lP_tp7hgNWT");
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;

            case 6: uri = Uri.parse("https://www.youtube.com/watch?v=KKde5jWu57g&index=5&list=PLIIhFdIwULxKicqb0dqI69lP_tp7hgNWT");
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;

            case 7: imageView = (ImageView) findViewById(R.id.surprise_image);
                res = getResources().getDrawable(R.drawable.surpriseme2);
                imageView.setImageDrawable(res);
                break;

            case 8: uri = Uri.parse("https://youtu.be/E6Vn17S37_Y");
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;

            case 9: imageView = (ImageView) findViewById(R.id.surprise_image);
                res = getResources().getDrawable(R.drawable.surpriseme3);
                imageView.setImageDrawable(res);
                break;
        }
    }
}
