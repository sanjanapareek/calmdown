package com.example.android.calmdown;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by sanjana.pareek on 27/05/16.
 */
public class MusicActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{

    ArrayAdapter<String> adapter;
    ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.music);
        String menuOptions[] = {"Yanni's Best", "Happy!", "Sky Full of Stars", "Fix You", "Paradise"
        };
        listView = (ListView) findViewById(R.id.music_list);
        adapter = new ArrayAdapter<>(this, R.layout.list_music, R.id.option_name, menuOptions);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Uri uri;
        Intent intent;
        switch (position){
            case 0:
                uri = Uri.parse("https://youtu.be/dE1o_uUXTvo"); // missing 'http://' will cause crashed
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;

            case 1:
                uri = Uri.parse("https://youtu.be/y6Sxv-sUYtM"); // missing 'http://' will cause crashed
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;


            case 2:
                uri = Uri.parse("https://youtu.be/LR73DrKX_bs"); // missing 'http://' will cause crashed
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;


            case 3:
                uri = Uri.parse("https://youtu.be/pY9b6jgbNyc"); // missing 'http://' will cause crashed
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;


            case 4:
                uri = Uri.parse("https://youtu.be/nSLSkRP6X3U"); // missing 'http://' will cause crashed
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;
        }

    }

}
