package com.example.android.calmdown;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    ArrayAdapter<String> adapter;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String menuOptions[] = { "Music",  "Video", "Meditate", "Jokes", "Surprise Me!"
        };
        listView = (ListView) findViewById(R.id.list_menu);
        adapter= new ArrayAdapter<>(this, R.layout.list_item, R.id.option_name, menuOptions);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

        Log.i("HelloListView", "You clicked Item: " + id + " at position:" + position);
        Intent intent ;

        switch(position){
            case 0 : intent = new Intent(this, MusicActivity.class);
                startActivity(intent);
                break;

            case 1 : intent = new Intent(this, VideoActivity.class);
                startActivity(intent);
                break;

            case 2 : intent = new Intent(this, MeditationActivity.class);
                startActivity(intent);
                break;

            case 3 : intent = new Intent(this, JokesActivity.class);
                startActivity(intent);
                break;

            case 4 : intent = new Intent(this, SurpriseActivity.class);
                startActivity(intent);
                break;
        }


    }
}
