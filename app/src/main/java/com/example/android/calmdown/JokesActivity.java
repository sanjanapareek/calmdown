package com.example.android.calmdown;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by sanjana.pareek on 27/05/16.
 */
public class JokesActivity extends Activity implements AdapterView.OnItemClickListener {
    ListView listView;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.jokes);
        String menuOptions[] = {"Yenna Rascala!", "The Doctor", "Lil Johny", "In the Bar", "DubSmash"};
        listView = (ListView) findViewById(R.id.jokes);
        adapter = new ArrayAdapter<String>(this, R.layout.list_jokes, R.id.option_name, menuOptions);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        Uri uri;
        Intent intent;

        switch (position){
            case 0:
                uri = Uri.parse("https://media.giphy.com/media/xT4uQn0ZAU1z5NPmve/giphy.gif");
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;

            case 1 :
                setContentView(R.layout.joke1);
                break;

            case 2 :
                setContentView(R.layout.joke2);
                break;

            case 3 :
                setContentView(R.layout.joke3);
                break;

            case 4 :
                setContentView(R.layout.joke4);
                break;
        }
    }
}
