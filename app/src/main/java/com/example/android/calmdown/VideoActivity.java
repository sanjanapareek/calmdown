package com.example.android.calmdown;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by sanjana.pareek on 27/05/16.
 */
public class VideoActivity extends Activity implements AdapterView.OnItemClickListener {

    ArrayAdapter<String> adapterView;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.video);
        String menuOptions[] = { "Thug Life!", "RaGa's Best", "Nana", "Arnab's Show", "Some Dubsmash"};

        listView = (ListView) findViewById(R.id.video_list);
        adapterView = new ArrayAdapter<>(this,R.layout.list_video, R.id.option_name, menuOptions);
        listView.setAdapter(adapterView);
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Intent intent ;
        Uri uri;

        switch(position){
            case 0 :  uri = Uri.parse("https://www.youtube.com/watch?v=hWMj6FSewgU"); // missing 'http://' will cause crashed
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;

            case 1 :  uri = Uri.parse("https://www.youtube.com/watch?v=igkax3bFlSs"); // missing 'http://' will cause crashed
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;

            case 2 :  uri = Uri.parse("https://www.youtube.com/watch?v=S1ljbecLSR0"); // missing 'http://' will cause crashed
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;

            case 3 :  uri = Uri.parse("https://www.youtube.com/watch?v=wAs7kags2pU"); // missing 'http://' will cause crashed
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;

            case 4 :  uri = Uri.parse("https://www.youtube.com/watch?v=KKde5jWu57g&index=5&list=PLIIhFdIwULxKicqb0dqI69lP_tp7hgNWT"); // missing 'http://' will cause crashed
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;


        }
    }
}
