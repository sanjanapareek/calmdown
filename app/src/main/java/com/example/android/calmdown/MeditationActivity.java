package com.example.android.calmdown;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by sanjana.pareek on 27/05/16.
 */
public class MeditationActivity extends Activity implements AdapterView.OnItemClickListener {

    ListView listView;
    ArrayAdapter<String> adapterView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.meditation);
        String menuOptions[] = {"Body Scan", "Relax", "Meditate with Om", "Flute Away", "Trance"};

        listView = (ListView) findViewById(R.id.meditation);
        adapterView = new ArrayAdapter<String>(this, R.layout.meditation_list, R.id.list_menu, menuOptions);
        listView.setAdapter(adapterView);
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Intent intent;
        Uri uri;
        switch (position){
            case 0: uri = Uri.parse("https://www.youtube.com/watch?v=CFONFsnqvD8");
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;

            case 1: setContentView(R.layout.meditation_1);
                break;

            case 2: uri = Uri.parse("https://www.youtube.com/watch?v=xNL_iYrpojw");
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;

            case 3: uri = Uri.parse("https://youtu.be/VeKRX7eC2To");
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;

            case 4: uri = Uri.parse("https://youtu.be/onquKz5SYRs");
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;
        }
    }
}
